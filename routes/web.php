<?php

use App\Http\Controllers\ManagerController;
use App\Http\Controllers\TestsResourceController;
use App\Http\Controllers\UsersResourceController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Homecontroller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::post('/estimate/{id}', [ManagerController::class, 'estimate']);

Route::resource('users', UsersResourceController::class)->only([
    'index', 'store', 'update', 'destroy'])
    ->names(
        [
            'index' => 'users',
            'store' => 'users.store',
            'update' => 'users.update',
            'destroy' => 'users.destroy',
        ]);


Route::resource('tests', TestsResourceController::class)->only([
    'index', 'store', 'update', 'destroy'])
    ->names(
        [
            'index' => 'tests',
            'store' => 'tests.store',
            'update' => 'tests.update',
            'destroy' => 'tests.destroy',
        ]);

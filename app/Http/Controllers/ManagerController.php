<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;

class ManagerController extends Controller
{


    public function estimate(Request $request, $id)
    {
        $criteria = 0;
        $rate = (int)$request->evalution;

        $collection = collect([100 => 60, 200 => 80, 300 => 91, 500 => 100]);

        $collection->each(function ($item, $key) use (&$criteria, &$rate) {
            if ($item >= $rate) {
                $criteria = $key;
                return false;
            }
        });

        $test = Test::find($id);
        $test->rate = $request->evalution;
        $test->criteria = $criteria;
        $test->save();

        return redirect()->route('tests');
    }
}

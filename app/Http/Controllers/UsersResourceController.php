<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UsersResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $roles = Role::all();
        $permissions = Permission::all();
        return response()->view('administration.users',
            [
                'users' => $users,
                'roles' => $roles,
                'permissions' => $permissions,
            ]);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::where('slug', $request->modalEditUserRole)->first();
        $user = User::create([
            'first_name' => $request->modalEditUserFirstName,
            'last_name' => $request->modalEditUserLastName,
            'email' => $request->modalEditUserEmail,
            'password' => bcrypt($request->modalEditUserPassword)
        ]);
        $user->roles()->attach($role);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::where('slug', $request->modalEditUserRole)->first();
        $user = User::where('id', $id)->first();
        $user->first_name = $request->modalEditUserFirstName;
        $user->last_name = $request->modalEditUserLastName;
        $user->email = $request->modalEditUserEmail;
        $user->password = bcrypt($request->modalEditUserPassword);
        $user->save();
        $user->roles()->attach($role);

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return $this->index();
    }
}

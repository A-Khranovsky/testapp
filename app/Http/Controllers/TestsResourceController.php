<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Test;
use App\Models\User;
use Illuminate\Http\Request;

class TestsResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all();
        $managers = Role::where('slug', 'manager')->get();
        return response()->view('administration.tests',
            [
                'tests' => $tests,
                'managers' => $managers
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->actionDate);
        Test::create([
            'fullName' => $request->modalEditUserFullName,
            'location' => $request->location,
            'testDate' => $request->actionDate,
            'user_id' => $request->responsable,
        ]);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = Test::find($id);

        $test->fullName = $request->modalEditUserFullName;
        $test->location = $request->location;
        $test->testDate = $request->actionDate;
        $test->user_id = $request->responsable;
        $test->save();

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = Test::find($id);
        $test->delete();
        return $this->index();
    }
}

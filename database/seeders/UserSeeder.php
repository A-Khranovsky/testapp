<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug','admin')->first();
        $manager = Role::where('slug', 'manager')->first();

        $crudManager = Permission::where('slug','crud-for-manager')->first();
        $crudTest = Permission::where('slug','crud-for-test')->first();
        $makeRate = Permission::where('slug','make-rate')->first();

        $admin->permissions()->attach($crudManager);
        $admin->permissions()->attach($crudTest);
        $manager->permissions()->attach($makeRate);


        $user1 = new User();
        $user1->first_name = 'Jhon';
        $user1->last_name = ' Deo';
        $user1->email = 'jhon@deo.com';
        $user1->password = bcrypt('secret');
        $user1->save();
        $user1->roles()->attach($admin);
//        $user1->permissions()->attach($crudManager);
//        $user1->permissions()->attach($crudTest);


        $user2 = new User();
        $user2->first_name = 'Mike';
        $user2->last_name = 'Thomas';
        $user2->email = 'mike@thomas.com';
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->roles()->attach($manager);
//        $user2->permissions()->attach($makeRate);
    }
}

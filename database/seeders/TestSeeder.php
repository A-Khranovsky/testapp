<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Test;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test= new Test();
        $test->fullName = 'Oren Reilly';
        $test->testDate = date("Y-m-d");
        $test->rate = 80;
        $test->location = 'Florida';
        $test->criteria = 200;
        $test->user_id = Role::where('slug','manager')->first()->users()->first()->id;
        $test->save();
    }
}

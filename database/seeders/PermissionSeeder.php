<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crudManager = new Permission();
        $crudManager->name = 'CRUD for manager';
        $crudManager->slug = 'crud-for-manager';
        $crudManager->save();

        $crudTests = new Permission();
        $crudTests->name = 'CRUD for test';
        $crudTests->slug = 'crud-for-test';
        $crudTests->save();

        $makeRate = new Permission();
        $makeRate->name = 'Make rate';
        $makeRate->slug = 'make-rate';
        $makeRate->save();
    }
}

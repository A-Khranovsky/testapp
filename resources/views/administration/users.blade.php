@extends('administration.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Users</h4>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        @role('admin')
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#editUser">#</button>
                        @endrole
                        @role('manager')
                            #
                        @endrole
                    </th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>E-mail</th>
                    @role('admin')
                    <th>Role</th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            @role('admin')
                                <a href="" data-bs-toggle="modal" data-bs-target="#editUser{{$user->id}}">{{$user->id}}</a>
                            @endrole
                            @role('manager')
                                {{$user->id}}
                            @endrole
                        </td>
                        <td>{{$user->first_name}}</td>
                        <td>{{$user->last_name}}</td>
                        <td>{{$user->email}}</td>
                        {{--                        <td>{{route()}}</td>--}}
                        @role('admin')
                        <td>{{$user->roles()->first()->name}}</td>
                        @endrole
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-5 px-sm-5 pt-50">
                    <div class="text-center mb-2">
                        <h1 class="mb-1">Create New User</h1>
                    </div>
                    <form method="POST" action="/users" id="editUserForm" class="row gy-1 pt-75">
                        @csrf
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserFirstName">First Name</label>
                            <input type="text" id="modalEditUserFirstName" name="modalEditUserFirstName"
                                   class="form-control" data-msg="Please enter your first name"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserLastName">Last Name</label>
                            <input type="text" id="modalEditUserLastName" name="modalEditUserLastName"
                                   class="form-control" data-msg="Please enter your last name"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserPassword">Password</label>
                            <input type="password" id="modalEditUserPassword" name="modalEditUserPassword"
                                   class="form-control"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserEmail">Billing Email:</label>
                            <input type="text" id="modalEditUserEmail" name="modalEditUserEmail" class="form-control"
                                   placeholder="example@domain.com"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserRoles">Roles</label>
                            <select id="modalEditUserRoles" name="modalEditUserRole" class="form-select"
                                    aria-label="Default select example">
                                @foreach($roles as $role)
                                    <option value="{{$role->slug}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @foreach($users as $user)
        <div class="modal fade" id="editUser{{$user->id}}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Edit User</h1>
                        </div>
                        <form method="POST" action="/users/{{$user->id}}" id="editUserForm" class="row gy-1 pt-75">
                            <input type="hidden" name="_method" value="PATCH">
                            @csrf
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserFirstName">First Name</label>
                                <input type="text" id="modalEditUserFirstName" name="modalEditUserFirstName"
                                       class="form-control" data-msg="Please enter your first name"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserLastName">Last Name</label>
                                <input type="text" id="modalEditUserLastName" name="modalEditUserLastName"
                                       class="form-control" data-msg="Please enter your last name"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserPassword">Password</label>
                                <input type="password" id="modalEditUserPassword" name="modalEditUserPassword"
                                       class="form-control"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserEmail">Billing Email:</label>
                                <input type="text" id="modalEditUserEmail" name="modalEditUserEmail" class="form-control"
                                       placeholder="example@domain.com"/>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserRoles">Roles</label>
                                <select id="modalEditUserRoles" name="modalEditUserRole" class="form-select"
                                        aria-label="Default select example">
                                    @foreach($roles as $role)
                                        <option value="{{$role->slug}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 text-center mt-2 pt-50">
                                <button type="submit" class="btn btn-primary me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                        aria-label="Close">
                                    Discard
                                </button>
                                <button type="button" data-bs-toggle="modal" data-bs-target="#danger{{$user->id}}" class="btn btn-danger me-1">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-danger text-start" id="danger{{$user->id}}" tabindex="-1"
             aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <form method="POST" action="/users/{{$user->id}}">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel120">Deleting</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            Are you sure that you want to delete user?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger btn-sm" data-bs-dismiss="modal">Yes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
@endsection

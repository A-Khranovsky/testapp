@extends('administration.layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Tests</h4>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>@role("admin")
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#editUser">#
                        </button>
                        @endrole
                        @role("manger")
                        #
                        @endrole
                    </th>
                    <th>Full name</th>
                    <th>Action date</th>
                    <th>Rate</th>
                    <th>Location</th>
                    <th>Criteria</th>
                    @role("admin")
                    <th>Responsable</th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                @foreach($tests as $test)
                    <tr>
                        <td>
                            <a href="" data-bs-toggle="modal" data-bs-target="#editTest{{$test->id}}">{{$test->id}}</a>
                        </td>
                        <td>{{$test->fullName}}</td>
                        <td>{{$test->testDate}}</td>
                        <td>{{$test->rate}}</td>
                        <td>{{$test->location}}</td>
                        <td>{{$test->criteria}}</td>
                        @role("admin")
                        <td>{{$test->user->first_name}} {{$test->user->last_name}}</td>
                        @endrole
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="editUser" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
            <div class="modal-content">
                <div class="modal-header bg-transparent">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body pb-5 px-sm-5 pt-50">
                    <div class="text-center mb-2">
                        <h1 class="mb-1">Create New Test</h1>
                    </div>
                    <form method="POST" action="/tests" id="editUserForm" class="row gy-1 pt-75">
                        @csrf
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="modalEditUserFullName">Full name</label>
                            <input type="text" id="modalEditUserFullName" name="modalEditUserFullName"
                                   class="form-control" data-msg="Please enter your full name"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="location-select">Location</label>
                            <div class="mb-1">
                                <select class="select2 form-select" name="location" id="location-select">
                                    <option value="EU">EU</option>
                                    <option value="SA">SA</option>
                                    <option value="NA">NA</option>
                                    <option value="AS">AS</option>
                                    <option value="AF">AF</option>
                                    <option value="AU">AU</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <label class="form-label" for="fp-default">Action date</label>
                            <input type="text" name="actionDate" id="fp-default" class="form-control flatpickr-basic"
                                   placeholder="YYYY-MM-DD"/>
                        </div>
                        <div class="col-12 col-md-6">
                            <label class="form-label" for="manager-select">Manager</label>
                            <div class="mb-1">
                                <select class="select2 form-select" name="responsable" id="manager-select">
                                    @foreach($managers as $manager)
                                        @foreach($manager->users as $user)
                                            <option
                                                value="{{$user->id}}">{{$user->first_name.' '.$user->last_name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 text-center mt-2 pt-50">
                            <button type="submit" class="btn btn-primary me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                Discard
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @foreach($tests as $test)
        <div class="modal fade" id="editTest{{$test->id}}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">
                                @role("admin")
                                Edit Test
                                @endrole
                                @role("manager")
                                Evaluation
                                @endrole
                            </h1>
                        </div>
                        @role("admin")
                        <form method="POST" action="/tests/{{$test->id}}" id="editTestForm" class="row gy-1 pt-75">
                            <input type="hidden" name="_method" value="PATCH">
                        @endrole
                        @role("manager")
                            <form method="POST" action="/estimate/{{$test->id}}" id="editTestForm" class="row gy-1 pt-75">
                        @endrole
                            @csrf
                            @role("admin")
                                <div class="col-12 col-md-6">
                                    <label class="form-label" for="modalEditUserFullName">Full name</label>
                                    <input type="text" id="modalEditUserFullName" name="modalEditUserFullName"
                                           class="form-control" data-msg="Please enter your full name"/>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label class="form-label" for="location-select">Location</label>
                                    <div class="mb-1">
                                        <select class="select2 form-select" name="location" id="location-select">
                                            <option value="EU">EU</option>
                                            <option value="SA">SA</option>
                                            <option value="NA">NA</option>
                                            <option value="AS">AS</option>
                                            <option value="AF">AF</option>
                                            <option value="AU">AU</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="fp-default">Action date</label>
                                    <input type="text" name="actionDate" id="fp-default"
                                           class="form-control flatpickr-basic"
                                           placeholder="YYYY-MM-DD"/>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label class="form-label" for="manager-select">Manager</label>
                                    <div class="mb-1">
                                        <select class="select2 form-select" name="responsable" id="manager-select">
                                            @foreach($managers as $manager)
                                                @foreach($manager->users as $user)
                                                    <option
                                                        value="{{$user->id}}">{{$user->first_name.' '.$user->last_name}}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endrole
                            @role("manager")
                                <div class="col-12">
                                    <label class="form-label" for="evalution-select">Evolution</label>
                                    <div class="mb-1">
                                        <select class="select2 form-select" name="evalution" id="evalution-select">
                                            @for($i=0; $i<=100; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            @endrole
                            <div class="col-12 text-center mt-2 pt-50">
                                <button type="submit" class="btn btn-primary me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary me-1" data-bs-dismiss="modal"
                                        aria-label="Close">
                                    Discard
                                </button>
                                @role("admin")
                                <button type="button" data-bs-toggle="modal" data-bs-target="#danger{{$test->id}}"
                                        class="btn btn-danger me-1">Delete
                                </button>
                                @endrole
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-danger text-start" id="danger{{$test->id}}" tabindex="-1"
             aria-labelledby="myModalLabel120" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <form method="POST" action="/tests/{{$test->id}}">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel120">Deleting</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            Are you sure that you want to delete test?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger btn-sm" data-bs-dismiss="modal">Yes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection

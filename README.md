## To run app use docker. Up the services:
docker-compose up -d

## Go to the container
docker exec -it testapp-php-apache-1 bash

## Run migrations and seeders
php artisan migrate:fresh --seed

## In the web-browser visit http://localhost.
### There are 2 accounts with administrator's role and manager's role

* Administrator's credentials:  
  Login: jhon@deo.com  
  Password: secret  
* Manager's credentials:  
  Login: mike@thomas.com  
  Password: secret  

## Leave container and down the services if you are exit
press ctrl+d  
docker-compose down
